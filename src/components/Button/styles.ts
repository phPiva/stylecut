import styled from 'styled-components';
import { StyledComponent } from 'styled-components';
export const Container: StyledComponent<
    'button',
    any,
    {},
    never
> = styled.button``;
